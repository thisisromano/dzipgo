from lxml import etree
import lxml.etree


def parse_xml(path_to_file):

    with open(path_to_file) as fobj:
        xml = fobj.read()

    root = etree.fromstring(xml)

    book_info_list = []

    books = root.getchildren()

    for book in books:
        book_info = {}
        book_info['id'] = book.get('id')
        for appt in book.getchildren():
            book_info[appt.tag] = appt.text
        book_info_list.append(book_info)

    return book_info_list


def info_xml(path_to_file):

    doc = lxml.etree.parse(path_to_file)
    count = doc.xpath('count(//book)')

    return int(count)
