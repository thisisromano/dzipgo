from django.shortcuts import render
import os
from django.core.files.storage import FileSystemStorage
import zipfile
from helper.xml import parse_xml, info_xml
from django.http import HttpResponse


def index(request):
    if request.method == "POST":
        uploaded_file = request.FILES['zip']

        fs = FileSystemStorage()
        fs.save(uploaded_file.name, uploaded_file)

        z = zipfile.ZipFile(uploaded_file, 'r')
        path_to_file = z.extract('test1.xml', path=os.path.join('media'))
        z.close()

        path = os.path.join('media', uploaded_file.name)
        os.remove(path)
        returned_text = parse_xml(path_to_file)
        about_file = info_xml(path_to_file)

        request.session['books'] = returned_text

        context = {'books': returned_text,
                   'about_file': about_file,
                   }

        return render(request, 'zip/display_xml.html', context)
    else:
        return render(request, 'zip/index.html')


def book(request, id):

    for i in request.session['books']:
        if i.get('id') == str(id):
            booking = i

    context = {'id': id,
               'booking': booking}

    return render(request,  'zip/book_info.html', context)


def author(request, author):

    allbooks = []
    for i in request.session['books']:
        if i.get('author') == str(author):
            allbooks.append(i.get('title'))

    context = {'author': author,
               'allbooks': allbooks,
               }

    return render(request, 'zip/about_author.html', context)
